# doorbirdpy

Python wrapper for [DoorBird LAN API v0.36](https://www.doorbird.com/downloads/api_lan.pdf)

[View on PyPI](https://pypi.org/project/DoorBirdPy/)

# Features

## Supported

- Live video request
- Live image request
- Open door/other relays
- Light on
- History image requests
- Schedule requests
- Favorites requests
- Check request
- Info request
- RTSP
- Monitor request

## Not yet supported

- Live audio transmit
- Live audio receive
- SIP

# Contributors

- @klikini
- @oblogic7
- @bdraco
- [alandtse](https://github.com/alandtse)
