Contributions are welcome, including bug fixes and new features within
scope, and especially updates to ensure compatibility with new DoorBird
API versions. 

# Code style

![code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)

Line length: 120 characters
